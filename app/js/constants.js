'use strict';

const AppSettings = {
    appTitle: 'Example Application',
    apiUrl: {
        base:'http://192.168.0.45:3000/',
        service:{
            signin:'signin',
            getGaps:'findGroup',
            joinGap:'addUserGroup',
            createGap:'addGroup'
        }
    },
    googleReverseGeolocationApi: 'https://maps.googleapis.com/maps/api/geocode/json?',
    mapConfig: {
        center: null,
        zoom: 15,
        minZoom: 3,
        draggableCursor: 'pointer',
        streetViewControl: false,
        scaleControl: true,
        rotateControl: true,
        panControl: false,
        zoomControl: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.SATELLITE
    }
};

export default AppSettings;
