'use strict';

function SigninCtrl(SigninService, ReverseGeolocationService, $scope, $location) {

  var vm = this;
  vm.showSuccess = false;
  vm.user = {
    name:null,
    username: null,
    email: null,
    password: null,
    address:'Via Eugenio Clara, 30, Chivasso, Torino',
    coords: [0, 0]
  };

  vm.register = function(){
     SigninService.register(vm.user)
     .then(function(result){
        localStorage.setItem('user', JSON.stringify(vm.user) );
        vm.showSuccess = true;
        $location.path('/gapselect');
        $scope.$apply();
     });
  };

  vm.reverseGeolocate = function(){
    ReverseGeolocationService.get(vm.user.address)
    .then(function(userCoords){
      vm.user.coords = userCoords;
      $scope.$apply();
  	});
  };

};

export default {
  name: 'SigninCtrl',
  fn: SigninCtrl
};
