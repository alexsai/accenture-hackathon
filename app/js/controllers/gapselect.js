'use strict';

function GapselectCtrl(GapService, $scope, $location) {
  var vm = this;

  GapService.getGaps()
    .then(function (gaps) {
      vm.gaps = gaps;
      $scope.$apply();
    });

  vm.join = function(gap){
    console.log('join gap:', gap);
    var user = localStorage.getItem('user');
    var joinGapData = {
      user:user,
      gap:gap._id
    };

    GapService.joinGap(joinGapData)
    .then(function (data) {
      $location.path('/chestselect');
      $scope.$apply();
    });
  }  

  vm.create = function(gapName){
    GapService.createGap(gapName)
    .then(function (data) {
      $location.path('/chestselect');
      $scope.$apply();
    });
  }

};

export default {
  name: 'GapselectCtrl',
  fn: GapselectCtrl
};
