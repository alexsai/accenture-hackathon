'use strict';

function ChestSelectCtrl(GapService, $scope, $location) {
  var vm = this;

  GapService.getGaps()
    .then(function (gaps) {
      vm.gaps = gaps;
      $scope.$apply();
    });
    

    vm.groupName = 'Membri';

    vm.gapMembers = [{
      id: 1,
      name:'Jacopo',
      selected: true
    },{
      id: 2,
      name:'Cecilia',
      selected: true
    },{
      id: 3,
      name:'Alexander',
      selected: true
    }];

};

export default {
  name: 'ChestSelectCtrl',
  fn: ChestSelectCtrl
};
