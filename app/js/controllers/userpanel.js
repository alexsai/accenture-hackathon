'use strict';

function UserpanelCtrl(ExampleService, $scope) {
  // ViewModel
  var vm = this;
  vm.mapCenter = {
    lat: 45.1975785,
    lng: 7.7977837
  };
  vm.user = {
    name:null,
    username: null,
    email: null,
    password: null,
    address:'1600 Amphitheatre Parkway, Mountain+View, CA',
    coords: {lat:0, lng:0}
  };

};

export default {
  name: 'UserpanelCtrl',
  fn: UserpanelCtrl
};
