'use strict';

function OnConfig($stateProvider, $locationProvider, $urlRouterProvider) {

  $locationProvider.html5Mode(true);

  $stateProvider
  // .state('Home', {
  //   url: '/',
  //   controller: 'ExampleCtrl as home',
  //   templateUrl: 'home.html',
  //   title: 'Home'
  // })
  .state('signin', {
    url: '/',
    controller: 'SigninCtrl as signin',
    templateUrl: 'signin.html',
    title: 'Signin'
  })
  .state('userpanel', {
    url: '/userpanel',
    controller: 'UserpanelCtrl as userpanel',
    templateUrl: 'userpanel.html',
    title: 'Userpanel'
  })
  .state('gapselect', {
    url: '/gapselect',
    controller: 'GapselectCtrl as gapselect',
    templateUrl: 'gapselect.html',
    title: 'Gapselect'
  })
  .state('chestselect', {
    url: '/chestselect',
    controller: 'ChestSelectCtrl as chestselect',
    templateUrl: 'chestselect.html',
    title: 'Chestselect'
  });

  $urlRouterProvider.otherwise('/');

}

export default OnConfig;