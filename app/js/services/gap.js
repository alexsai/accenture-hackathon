'use strict';

function GapService($http, AppSettings) {
  const service = {};

  service.getGaps = function() {
    return new Promise((resolve, reject) => {
      var apiUrl = AppSettings.apiUrl.base + AppSettings.apiUrl.service.getGaps;
      $http.get(apiUrl).success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

  service.joinGap = function(gap) {
    return new Promise((resolve, reject) => {
      var apiUrl = AppSettings.apiUrl.base + AppSettings.apiUrl.service.joinGap;
      $http.post(apiUrl,gap).success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

  service.createGap = function(name) {
    return new Promise((resolve, reject) => {
      var apiUrl = AppSettings.apiUrl.base + AppSettings.apiUrl.service.createGap;
      $http.post(apiUrl,{name:name}).success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

  return service;
  };



export default {
  name: 'GapService',
  fn: GapService
};