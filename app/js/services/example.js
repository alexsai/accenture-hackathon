'use strict';

function ExampleService($http, AppSettings) {
  const service = {};

  service.get = function() {
    return new Promise((resolve, reject) => {
      $http.get(AppSettings.apiUrl).success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };
  
  return service;
};


export default {
  name: 'ExampleService',
  fn: ExampleService
};