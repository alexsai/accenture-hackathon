'use strict';

function SigninService($http, AppSettings) {
  const service = {};

  service.register = function(user) {
    return new Promise((resolve, reject) => {
      var apiUrl = AppSettings.apiUrl.base + AppSettings.apiUrl.service.signin;
      $http.post(apiUrl,user).success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };
  
  return service;
}


export default {
  name: 'SigninService',
  fn: SigninService
};