'use strict';

function ReverseGeolocationService($http, AppSettings) {
  const service = {};

  service.get = function(address) {
    return new Promise((resolve, reject) => {
      $http({
        url: AppSettings.googleReverseGeolocationApi,
        method: 'GET',
        params: { address: address },
        transformResponse: appendTransform($http.defaults.transformResponse, function(response) {
          if(response.status === 'OK'){
            return response.results[0].geometry.location;
          }
          return 0;
        })
      }).
      success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };
  
  function appendTransform(defaults, transform) {
      defaults = angular.isArray(defaults) ? defaults : [defaults];
      return defaults.concat(transform);
  }


  return service;
  };



export default {
  name: 'ReverseGeolocationService',
  fn: ReverseGeolocationService
};