'use strict';

function ExampleDirective() {

  return {
    restrict: 'EA',
    templateUrl: 'directives/example.html',
    scope: {
      title: '@',
      message: '@exampleDirective'
    },
    link: (scope, element) => {

    }
  };
};

export default {
  name: 'exampleDirective',
  fn: ExampleDirective
};