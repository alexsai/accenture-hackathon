'use strict';

var _ = require('lodash');

function MapDirective(AppSettings) {
    return {
        restrict: 'E',
        templateUrl: 'directives/map.html',
        scope: {
            mapCenter: '='
        },
        link: (scope, element) => {
            var mapConfig = AppSettings.mapConfig;
            mapConfig.center = scope.mapCenter;

            var map = new google.maps.Map(element[0].querySelector('#map-holder'), mapConfig);
            var marker = new google.maps.Marker({
                position: scope.mapCenter,
                map: map,
                icon: 'images/farm-marker.png'
              });
        }
    };
};

export default {
    name: 'mapDirective',
    fn: MapDirective
};


