'use strict';

function MenuDirective() {

  return {
    restrict: 'E',
    templateUrl: 'directives/menu.html',
    scope: {

    },
    link: (scope, element) => {
    }
  };
};

export default {
  name: 'menuDirective',
  fn: MenuDirective
};